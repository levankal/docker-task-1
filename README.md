# Docker Task 1

## build image, run container and test web server

![build image and run container](screenshots/image.png)

## check nginx version and test web server inside container
![test inside container](screenshots/image-1.png)

## dockerfile, nginx instalation script and nginx config file 
![config files](screenshots/image-2.png)