FROM ubuntu:latest
COPY nginx.sh /
RUN chmod +x /nginx.sh && /nginx.sh && rm /nginx.sh
COPY index.html /usr/share/nginx/html/index.html
COPY test.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]